<?php
/* 
 * Remember that this file is only used if you have chosen to override event pages with formats in your event settings!
 * You can also override the single event page completely in any case (e.g. at a level where you can control sidebars etc.), as described here - http://codex.wordpress.org/Post_Types#Template_Files
 * Your file would be named single-event.php
 */
/*
 * This page displays a single event, called during the the_content filter if this is an event page.
 * You can override the default display settings pages by copying this file to yourthemefolder/plugins/events-manager/templates/ and modifying it however you need.
 * You can display events however you wish, there are a few variables made available to you:
 * 
 * $args - the args passed onto EM_Events::output() 
 */

global $current_user;
get_currentuserinfo();

global $EM_Event;
/* @var $EM_Event EM_Event */
echo $EM_Event->output_single();
?>

<?php if(is_user_logged_in()): ?>
<?php echo do_shortcode('[wpRTC room_name="'.$EM_Event->post_title.'"]') ?>



<header>
  <button onclick="joinRoom()">Join room</button>
  <button onclick="leaveRoom()">Leave room</button>
  <br/>
  <input type="text" id="message" placeholder="My message" />
  <button onclick="sendMessage()">Send message</button>
</header>

<div id="container">
  <div id="chatbox" style="min-height:100px"></div>
</div>

<script type="text/javascript">
  var roomConfig = {
    user_name: '<?php echo $current_user->display_name ?>',
    room_name: '<?php echo $EM_Event->post_title ?>'
  };

  var skylink = new Skylink();
  jQuery(window).load(function() {


      skylink.init('6414dde6-47c9-4502-aef5-695a2870d392');

      skylink.setUserData({
        name: roomConfig.user_name
      });

      joinRoom();

      skylink.on('peerJoined', function(peerId, peerInfo, isSelf) {
        var user = 'You';
        if(!isSelf) {
          user = peerInfo.userData.name || peerId;
        }
        addMessage(user + ' joined the room', 'action');
      });

      skylink.on('peerUpdated', function(peerId, peerInfo, isSelf) {
        if(isSelf) {
          user = peerInfo.userData.name || peerId;
          addMessage('You\'re now known as ' + user, 'action');
        }
      });

      skylink.on('peerLeft', function(peerId, peerInfo, isSelf) {
        var user = 'You';
        if(!isSelf) {
          user = peerInfo.userData.name || peerId;
        }
        addMessage(user + ' left the room', 'action');
      });

      skylink.on('incomingMessage', function(message, peerId, peerInfo, isSelf) {
        var user = 'You',
          className = 'you';
        if(!isSelf) {
          user = peerInfo.userData.name || peerId;
          className = 'message';
        }
        addMessage(user + ': ' + message.content, className);
      });




  });
  function joinRoom() {
    skylink.joinRoom(roomConfig.room_name);
  }
  function leaveRoom() {
    console.log(skylink.roomName+' - left');
    skylink.leaveRoom();
  }
  function sendMessage() {
    var input = document.getElementById('message');
    skylink.sendP2PMessage(input.value);
    input.value = '';
  }
  function addMessage(message, className) {
    var chatbox = document.getElementById('chatbox'),
      div = document.createElement('div');
    div.className = className;
    div.textContent = message;
    chatbox.appendChild(div);
  }
</script>

<?php endif; ?>